const express = require('express')
const parser = require('body-parser')
var MongoClient = require('mongodb').MongoClient
const ejs = require('ejs')

const server = express();
const port = 3000;
server.set('view engine', 'ejs');
let db;

MongoClient.connect("mongodb://localhost:27017",
            function(err, client){
                if(err){
                    console.log(err);
                }else{
                    console.log('db assigned')
                    db = client.db('test')
                }
            });

server.use(parser.urlencoded({extended: true}));
server.use('/', express.static(__dirname+'/public'));
console.log(db);





server.get('/test/saveREST/:fname/:lname', function(req, res){
    let firstname = req.params.fname;
    let lastname = req.params.lname;
    let collection = db.collection('names');
    collection.insertOne({'fname': firstname, 'lname': lastname}, (err, result)=>{
        if(err){
            console.log(err);
        }else{
            console.log('Successfully added 1')
        }
    })
    console.log("Firstname: "+firstname+" Lastname: "+ lastname);
    res.send({'fname': firstname, 'lname': lastname});
})

server.get('/test/showAll', function(req, res){
    let collection = db.collection('names');
    let ausgabe ="";
    collection.find().toArray((err, items)=>{
        res.render('table.ejs', {'quotes': items});
    })
    //console.log(string);
    //res.send(string);
})
//
server.listen(port, ()=>console.log(`Example app listen on port ${port}`))