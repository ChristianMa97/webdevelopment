# Tasks to get familiar with javascript, express, jquery, mongodb and ejs
<br>

## Javascript without Server:

- calculator.html
    - basic calculator with is able to perform addition, multiplication, division, power and squareroot operations.
- changeStylesheet.html
    - using javascript to change the used stylesheet.
<br><br>
## Server related Tasks:

In the Folder "server" are server related examples using an express server.<br>
To start the server navigate inside the folder "server" and use "node index.js" in the terminal.(There must run a local mongodb service).<br>
**The sites need to be accessed via localhost and not via 127.0.0.1 due to same origin policy.**

Server

- ajax.html http://localhost:3000/ajax.html
    - basic ajax calls to the server.
- miniUebung.html http://localhost:3000/miniUebung.html
    - read data from MongoDB-Database and return result as Table.
- DatabaseTest.html http://localhost:3000/DatabaseTest.html
    - using an array inside the server as Database and give the functionality to save, delete and search data.



The Folder Project_EJS_MongoDB contains a simple Project to save credentials and print them as list via ejs.

- To start this server navigate into Project_EJS_MongoDB and use "node index.js"(there must be a running mongodb service and a collection named test).
- To open site use : http://localhost:3000/test.html