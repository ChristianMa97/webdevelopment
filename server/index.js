const express = require('express')
const parser = require('body-parser')
var MongoClient = require('mongodb').MongoClient;
let ejs = require('ejs')

const server = express()
const port = 3000
server.set('view engine', 'ejs');

var db;
var xml = require('xml');

MongoClient.connect("mongodb://localhost:27017",   
	function(err, client) {
		db= client.db('test');// Los geht's mit der Variablen db... 
	});

server.use(parser.urlencoded({extended: true}));
server.use('/', express.static(__dirname + '/public'));

server.get('/api', (req, res) => res.send('Hello World!'))

server.get('/lennart', function (req, res) {
	res.send('Hello from Lennart');
})

server.get('/chris', (req, res) => {
	res.send('Hello from Chris');
})
server.get('/message', function (req, res) {
	res.set('Content-Type', 'text/xml');
	res.send("<?xml version='1.0' encoding='utf-8' ?><message>Hello World!</message>");
})


//zugehörig ajax.html
server.post('/hello', function(req, res){
	let name_parameter = req.body.name;
	res.send({'response': 'Hallo '+ name_parameter + ', wie gehts?'});
})
//Selbes wie oben nur als REST mit param
server.post('/hello/:name', function(req, res){
	let name_parameter = req.params.name;
	res.send({'response': 'Hallo '+ name_parameter + ', wie gehts?'});
})
//optionales attribut nachname
server.post('/hello/:name/opt/:nachname', function(req, res){
	let name_parameter = req.params.name;
	let nachname = req.params.nachname;
	console.log(nachname);
	if(nachname == undefined){
		res.send({'response': 'Hallo '+ name_parameter + ', wie gehts?'});
	}else{
		res.send({'response': 'Hallo '+ name_parameter + ' ' + nachname + ', wie gehts?'});

	}


})
let database = [
	
];
//zugehörig DatabaseTest.html
server.post('/saveData', function(req,res){
	let id = req.body.id;
	let vorname = req.body.vorname;
	let name = req.body.name;
	let adresse = req.body.adresse;
	let phone = req.body.phone;
	database.push({'id': id,
				'vorname': vorname,
				'name': name,
				'adresse': adresse,
				'phone': phone})
	res.send({'response':'complete'});
})

server.post('/search', function(req,res){
	for(let i=0; i<database.length;i++){
		if(database[i].id == req.body.id){
			res.send(database[i]);
		}
	}

})
server.get('/getAllData', (req,res)=>{
	res.send(database);
})
server.post('/delete', function(req, res){
	let newDatabase=[];
	for(let i=0; i<database.length;i++){
		if(database[i].id != req.body.id){
			newDatabase.push(database[i])
		}
	}
	database = newDatabase;
	res.send(200);
})


//gehört zu miniUebung.html
server.get('/mongoDBTest', (req,res)=>{
	let cuisine = req.query.cuisine;
	let borough = req.query.borough;
	console.log(cuisine);
	console.log(borough);
	db.collection('restaurants').find({ cuisine: cuisine, borough: borough}).toArray(function(err, result){
		let ausgabe = "";
		result.forEach(element => {
			ausgabe+= "<tr><td>"+element.name+"</td><td>"+element.address.street+"</td></tr>";
		});
		console.log(ausgabe);
		res.send(ausgabe);
	});
})

server.get('/test', (req, res)=>{
	res.render('pages/index');

})




server.listen(port, () => console.log(`Example app listening on port ${port}!`))
